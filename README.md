# About

The purpose of this repository is to be tested against secrets identification
tools such as [gitLeaks](https://github.com/zricethezav/gitleaks).

# Links

1. [DevOps.Toys - Identifying Secrets in Your Source Control Management Repository](https://www.devops.toys/1_sec/IdentifyingSecretsinYourSourceControlManagementRepository)